package com.newrelic.codingchallenge;

/**
 * The Util class contains some utility functions used elsewhere
 * in the package.
 *
 * @author  Brandon Burnett (gentoolicious@gmail.com)
 * @version 1.0
 * @since   2018-03-21
 */
public class Util {
    /**
     * Returns true if the string received on the socket is the 'terminate' string
     *   e.g., terminate followed by a newline
     *
     * @param s The string to determine terminate'edness of
     * @return  True if the string is the 'terminate' string
     * @see     Boolean
     */
    public static boolean isTerminate(String s) {
        return s.matches("terminate\\R");
    }

    /**
     * Returns true if the string is formatted correctly
     *   e.g., a sequence of nine digits followed by a newline
     *
     * @param s The string to analyze
     * @return  True if the byte[] is formatted appropriate
     * @see     Boolean
     */
    public static boolean isFormattedCorrectly(String s) {
        return (s.matches("\\d{9}\\R") || isTerminate(s));
    }
}
