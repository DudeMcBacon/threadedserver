package com.newrelic.codingchallenge;

import java.io.BufferedWriter;
import java.io.IOException;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

/**
 * NumberSet is a small helper class used to keep track of
 * number uniqueness and also to help write to the log file
 * storing the unique numbers.
 *
 * Number uniqueness is determined by adding all valid, non
 * 'terminate' requests received from a client to a Set.
 * Since uniqueness is inherent in a Set, no special logic
 * is required to determine number uniqueness. Additionally,
 * we can check that a number has already been added to the
 * set by using the Set.contains() instance method. This will
 * give us an indication of whether we're about to add a number
 * that has already been added to the set.
 *
 * NumberSet also contains several counters used by the
 * printSummary() instance method to report on the number of
 * additions, duplicates, and total unique set members.
 *
a * @author  Brandon Burnett (gentoolicious@gmail.com)
 * @version 1.0
 * @since   2018-03-21
 */
public class NumberSet {
    private int additions = 0;
    private int duplicates = 0;

    private Set<String> set = new HashSet<>();
    private Set<String> synchronizedSet = Collections.synchronizedSet(set);

    private BufferedWriter writer;

    public NumberSet(BufferedWriter writer) {
        this.writer = writer;
    }

    /**
     * Allows numbers to be added to the set. The function acts as a
     * convenience method doing several things at once:
     *
     *    1. The uniqueness of the number is check by seeing if the set
     *       already contains the number.
     *    2. If the set does not contain the number, it is added, and the
     *       additions counter is incremented by one.
     *    3. Additionally, the number is written to the log.
     *    4. If the number is already in the Set, then the duplicates
     *       counter is incremented by one.
     *
     * @param number The number to add to the Set
     */
    public void addToSet(String number) throws IOException {
        if (!this.synchronizedSet.contains(number)) {
            this.synchronizedSet.add(number);
            this.incrementAdditions();
            this.writeLineToLog(number);
            this.flush();
        } else {
            this.incrementDuplicates();
        }
    }

    /**
     * Flushes the BufferedWriter provided to NumberSet on
     * instantiation.
     */
    public void flush() throws IOException {
        this.writer.flush();
    }

    /**
     * Returns the additions counter.
     *
     * @return the additions counter
     * @see Integer
     */
    public int getAdditions() {
        return this.additions;
    }

    /**
     * Returns the duplicates counter
     *
     * @return the duplicates counter
     * @see Integer
     */
    public int getDuplicates() {
        return this.duplicates;
    }

    /**
     * Returns the size of the Set
     *
     * @return the size of the set
     * @see Integer
     */
    public int getSetSize() {
       return this.synchronizedSet.size();
    }

    /**
     * Increments the additions counter by one
     */
    public void incrementAdditions() {
       this.additions = this.additions + 1;
    }

    /**
     * Increments the duplicates counter by one
     */
    public void incrementDuplicates() {
       this.duplicates = this.duplicates + 1;
    }

    /**
     * Prints a summary of the additions, duplicates,
     * and total unique numbers in the Set. Also resets
     * the duplicates and additions counter.
     */
    public void printSummary() {
        System.out.println(String.format("Received %d unique numbers, %d duplicates. Unique total: %d",
                this.additions, this.duplicates, this.synchronizedSet.size()));
        this.additions = 0;
        this.duplicates = 0;
    }

    /**
     * Writes a number to the BufferWriter supplied
     * to NumberSet on instantiation.
     */
    private void writeLineToLog(String number) throws IOException {
        writer.write(number);
    }
}
