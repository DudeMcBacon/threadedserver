package com.newrelic.codingchallenge;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * The ThreadedNumbersServer implements an application and
 * class capable of fulfilling the requirements laid out
 * in the New Relic coding challenge.
 *
 * Better performance is achieved by delegating the processing of
 * new client connections to requests handlers. Also, a log
 * file is maintained containing the list of unique numbers
 * added to the set and is explicitly flush to disk periodic-
 * ally to maintain data resiliency.
 *
 * @author  Brandon Burnett (gentoolicious@gmail.com)
 * @version 1.0
 * @since   2018-03-21
 */
public class ThreadedNumbersServer {

    public static void main(String[] args) {
        // Control these with command line args, ideally.
        int port = 4000;
        int threads = 5;

        try {
            // Create a BufferedWriter and pass it into a new NumberSet
            BufferedWriter writer = new BufferedWriter(new FileWriter("numbers.log"));

            // NumberSet is basically just a synchronized Set with some extra functionality wrapped around it
            // to manage the uniqueness of strings supplied to it and writer to the log.
            NumberSet numberSet = new NumberSet(writer);

            ThreadedNumbersServer tns = new ThreadedNumbersServer(port, threads, numberSet);
            tns.startServer();

            // If the server is shutdown by the 'terminate' signal, make sure to close the writer
            // supplied to it. Otherwise, rely on the statsReporter to flush to disk periodically.
            writer.close();
        } catch (IOException e) {
            System.out.println("IOException: ");
            e.printStackTrace();
        }
    }

    private int port;
    private int threads;
    private NumberSet numberSet;
    private boolean isStopped;
    private ServerSocket serverSocket;

    public ThreadedNumbersServer(int port, int threads, NumberSet numberSet) {
        this.port = port;
        this.threads = threads;
        this.numberSet = numberSet;
    }

    public void startServer() throws IOException {
        System.out.println(String.format("ThreadedNumbersServer starting on port %d", port));


        // Create a fixed thread pool so that we don't exceed this.threads threads.
        ExecutorService executor = Executors.newFixedThreadPool(this.threads);

        // Using a ScheduledThreadPool will allow me to fairly accurately execute some code every n seconds. I will
        // use this to display the statistics required by the coding challenge.
        ScheduledExecutorService statsReporter = Executors.newScheduledThreadPool(1);
        statsReporter.scheduleAtFixedRate(() -> {
            // Print the summary of unique additions, duplicates, and total unique additions.
            numberSet.printSummary();

            try {
                // Flush the file to disk periodically -- I think this is a good medium between flushing after every write
                // and flushing only when the applications exits. Obviously the log file won't be as resilient to data loss
                // but the benefit is that I can easily maintain 2M additions to the Set ever 10s *and* if data is lost,
                // only the ~10s worth will be gone. If I flush after every write, I'll likely be much more resilient in
                // terms of data loss but I can only maintain about 100k set additions every 10s.
                numberSet.flush();
            } catch (IOException e) {
                System.out.println("IOException attempting to flush log file. Killing ThreadNumbersServer...");
                e.printStackTrace();
                this.stop();
            }
        }, 10, 10, TimeUnit.SECONDS);

        this.serverSocket = new ServerSocket(this.port);
        try {
            System.out.println("Waiting for clients...");

            while(!isStopped()) {
                Socket clientSocket;

                try {
                    // Since clientSocket is an instance variable I can close it with another method and then catch
                    // any IOExceptions when I try to accept() from it as an indication that the server should be
                    // shutting down.
                    clientSocket = this.serverSocket.accept();
                } catch (SocketException e) {
                    // Confirm that we've actually shutdown the server and that something else hasn't happened.
                    if(isStopped()) {
                        break;
                    }

                    // If we don't recognize the exception, return it.
                    throw new RuntimeException("Error accepting client connection", e);
                }

                // I feel like there may be a more idiomatic way to do this but I'm stretching my
                // knowledge of Java pretty thinly here. In any case, passing a reference to this
                // ThreadedNumbersServer will allow me to start call the stop() method from the
                // RequestHandler in the event that it receives a 'terminate' command.
                Runnable worker = new RequestHandler(clientSocket, numberSet, this);
                executor.execute(worker);
            }

            System.out.println("Shutting down server...");
            serverSocket.close();
            System.out.println(String.format("Serviced %d unique additions to the number set", numberSet.getSetSize()));
        } catch (IOException e) {
            System.out.println(e.getMessage());
        } finally {
            executor.shutdown();
            statsReporter.shutdown();
        }
    }

    /**
     * Returns true if ThreadedNumbersServer is "stopped".
     *
     * @return True if ThreadedNumbersServer is "stopped"
     * @see Boolean
     */

    public synchronized boolean isStopped() {
        return this.isStopped;
    }

    /**
     * Stop ThreadedNumbersServer from continuing to service requests by
     * flipping the isStopped instance variable and closing the serverSocket.
     *
     */
    public synchronized void stop() {
        this.isStopped = true;
        try {
            this.serverSocket.close();
        } catch (IOException e) {
            throw new RuntimeException("Error closing server", e);
        }
    }
}
