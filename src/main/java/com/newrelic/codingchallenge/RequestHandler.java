package com.newrelic.codingchallenge;

import java.io.IOException;
import java.io.InputStream;
import java.net.Socket;

/**
 * The RequestHandler class implements a Runnable that is
 * capable of processing requests received by
 * ThreadNumbersServer.
 *
 * @author  Brandon Burnett (gentoolicious@gmail.com)
 * @version 1.0
 * @since   2018-03-21
 */
public class RequestHandler implements Runnable {
    private NumberSet numberSet;
    private Socket clientSocket;
    private ThreadedNumbersServer tns;

    public RequestHandler(Socket clientSocket, NumberSet numberSet, ThreadedNumbersServer tns) {
        this.clientSocket = clientSocket;
        this.numberSet = numberSet;
        this.tns = tns;
    }

    @Override
    public void run() {
        try {
            while (true) {
                byte[] data = new byte[10];

                // Grab the input from the socket
                InputStream input = clientSocket.getInputStream();
                int numRead = -1;
                numRead = input.read(data);

                // If numRead is still -1 then we haven't received any new data. Disconnect.
                if (numRead == -1) {
                    System.out.println("Client did not send any data. Hanging up...");
                    input.close();
                    clientSocket.close();
                    return;
                }

                // Determine if the data sent by the client is formatted correctly. Disconnect if not.
                String theString = new String(data);
                if (!Util.isFormattedCorrectly(theString)) {
                    System.out.println("Client sent badly formatted data. Hanging up...");
                    input.close();
                    clientSocket.close();
                    return;
                }

                // Determine if the client has sent the 'terminate' signal. Shutdown ThreadedNumbersServer if so.
                if (Util.isTerminate(theString)) {
                    System.out.println("Client sent terminate signal. Shutting down ThreadedNumbersServer...");
                    input.close();
                    clientSocket.close();
                    tns.stop();
                    return;
                }

                numberSet.addToSet(theString);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
