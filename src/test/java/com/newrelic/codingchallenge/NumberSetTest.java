package com.newrelic.codingchallenge;

import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintStream;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class NumberSetTest {
    private NumberSet numberSet;

    @Before
    public void initialize() {
        try {
            BufferedWriter writer = new BufferedWriter(new FileWriter("test.log"));
            numberSet = new NumberSet(writer);
        } catch (IOException e) {
           System.out.println("Problem creating file.");
        }
    }

    @Test
    public void testAddToSet() {
        try {
            numberSet.addToSet(String.format("999999999%n"));
            numberSet.addToSet(String.format("999999999%n"));
            assertEquals(1, numberSet.getSetSize());
            assertEquals(1, numberSet.getDuplicates());
            numberSet.addToSet(String.format("999999998%n"));
            assertEquals(2, numberSet.getSetSize());
            assertEquals(2, numberSet.getAdditions());
        } catch (IOException e) {
            System.out.println("Problem in testAddToSet: ");
            e.printStackTrace();
        }
    }

    @Test
    public void testGetSetSize() {
        try {
            numberSet.addToSet(String.format("999999999%n"));
            assertEquals(1, numberSet.getSetSize());
        } catch (IOException e) {
            System.out.println("Problem in testGetSetSize: ");
            e.printStackTrace();
        }
    }

    @Test
    public void testIncrementAdditions() {
        numberSet.incrementAdditions();
        assertEquals(1, numberSet.getAdditions());
    }

    @Test
    public void testIncrementDuplicates() {
        numberSet.incrementDuplicates();
        assertEquals(1, numberSet.getDuplicates());
    }

    @Test
    public void testPrintSummary() {
        ByteArrayOutputStream outContent = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outContent));

        try {
            numberSet.addToSet(String.format("999999999%n"));
            numberSet.addToSet(String.format("999999999%n"));
            numberSet.addToSet(String.format("999999998%n"));
            numberSet.printSummary();
            assertEquals(String.format("Received 2 unique numbers, 1 duplicates. Unique total: 2%n"), outContent.toString());
        } catch (IOException e) {
            System.out.println("Problem in testPrintSummary: ");
            e.printStackTrace();
        }

        System.setOut(System.out);
    }

    @Test
    public void testWriteLineToLog() {
    }
}
