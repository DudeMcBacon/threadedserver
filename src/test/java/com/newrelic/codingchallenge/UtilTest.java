package com.newrelic.codingchallenge;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class UtilTest {
    @Test
    public void testIsTerminate() {
       String good = String.format("terminate%n");
       String bad = "terminate";
       assertEquals(true, Util.isTerminate(good));
       assertEquals(false, Util.isTerminate(bad));
    }

    @Test
    public void testIsFormattedCorrectly() {
        String good = String.format("999999999%n");
        String bad = "999999999";
        assertEquals(true, Util.isFormattedCorrectly(good));
        assertEquals(false, Util.isFormattedCorrectly(bad));
    }
}
