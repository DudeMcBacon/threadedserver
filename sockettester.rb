require 'socket'
require 'parallel'

# Test with a random data set
Parallel.each(1..5, in_processes: 5) do
  count = 0
  s = TCPSocket.new 'localhost', 4000
  loop do
    string = rand(999999999).to_s.rjust(9, "0") + "\n"
    s.puts string
    count = count + 1
    break if count > 50000
  end
  s.close
end

# Test with some duplicates
s = TCPSocket.new 'localhost', 4000
s.puts "999999999\n"
s.puts "999999999\n"
s.puts "999999999\n"
s.puts "999999999\n"
s.puts "999999999\n"
s.close

# Test with some bad data
s = TCPSocket.new 'localhost', 4000
s.puts "bunk\n"
s.puts "data\n"
s.close

# Test with terminate
s = TCPSocket.new 'localhost', 4000
s.puts "terminate\n"
s.close
